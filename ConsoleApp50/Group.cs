﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Project1
{
    class Group
    {
        public static string set_name = "";
        public string Name { get; set; }
        public string Teacher { get; set; }
        public List<Student> Students { get; set; }
    }
}
