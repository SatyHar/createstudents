﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Project1
{
    class MicMeneger
    {
        public Group[] CreateGroup(string subject, Teacher[] teachars, Student[] students)
        {
            Group[] groups = new Group[teachars.Length];

            for (int i = 0; i < teachars.Length; i++)
            {
                Group group = new Group
                {
                    Students = SublistStudents(students, teachars.Length, i),
                    Teacher = teachars[i].Name,
                    Name = subject
                };

                groups[i] = group;
            }
            return groups;
        }

        private static List<Student> SublistStudents(Student[] stud, int groupCount, int i)
        {
            int step = 0;
            int s = 0;
            int _mnacord = stud.Length % groupCount;
            int mnacord = _mnacord - i;

            List<Student> sublist = new List<Student>();

            if (mnacord >= 1)
            {
                s = i * (stud.Length / groupCount + 1);

                if (i == groupCount - 1)
                    step = i * (stud.Length / groupCount) + mnacord + 1;
                else
                    step = s + 1;
            }

            for (int l = s; l < stud.Length / groupCount + step; l++)
            {
                sublist.Add(stud[l]);
            }

            return sublist;
        }

        public void Shuffle(Student[] stud)
        {
            Random random = new Random();
            for (int i = stud.Length - 1; i > 0; i--)
            {
                int swapIndex = random.Next(i + 1);
                Swap(stud, i, swapIndex);
            }
        }

        private void Swap(Student[] stud, int i, int index)
        {
            Student temp = stud[i];
            stud[i] = stud[index];
            stud[index] = temp;
        }
    }
}