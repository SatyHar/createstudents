﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Project1
{
    class DataBase
    {
        public Student[] CreateStudent(int n)
        {
            Random rand = new Random();
            var students = new Student[n];
            for (int i = 0; i < students.Length; i++)
            {
                var student = new Student();
                student.Name = $"A{i + 1}";
                student.Surname = $"A{i + 1}yan";
                student.Email = $"A{i + 1}$gmail.com";
                student.Age = (byte)rand.Next(15, 60);
                students[i] = student;
            }
            return students;
        }

        public Teacher[] CreateTeacher(int n)
        {
            Random rand = new Random();
            var teachers = new Teacher[n];
            for (int i = 0; i < teachers.Length; i++)
            {
                var teacher = new Teacher();
                teacher.Name = $"B{i + 1}";
                teacher.Surname = $"B{i + 1}yan";
                teacher.Email = $"B{i + 1}$gmail.com";
                teacher.Age = (byte)rand.Next(26, 60);
                teacher.Salary = rand.Next(300000, 600000);
                teachers[i] = teacher;
            }
            return teachers;
        }
    }
}
