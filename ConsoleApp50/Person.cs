﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Project1
{
    class Person
    {
        public string Name { get; set; }
        public string Surname { get; set; }

        public string FullName
        {
            get { return $"{Surname} {Name}"; }
        }

        public string Email { get; set; }
        public byte Age { get; set; }

        public Person()
        {

        }
    }
}
