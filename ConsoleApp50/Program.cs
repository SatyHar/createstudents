﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Project1
{
    class Program
    {
        static void Main(string[] args)
        {
            DataBase database = new DataBase();
            Teacher[] teachers = database.CreateTeacher(7);
            Student[] students = database.CreateStudent(12);

            MicMeneger meneger = new MicMeneger();
            meneger.Shuffle(students);

            MicMeneger gr = new MicMeneger();
            Group[] groups = meneger.CreateGroup("C# courses", teachers, students);
            Print(groups);

            Console.ReadLine();

        }

        static void Print(Group[] groups)
        {
            for (int i = 0; i < groups.Length; i++)
            {
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine($"{"GROUP:"}{groups[i].Name}{"TEACHER NAME:"}{groups[i].Teacher}");
                Console.ResetColor();
                if (groups[i] != null)
                {
                    for (int j = 0; j < groups[i].Students.Count; j++)
                    {
                        Console.WriteLine($"{ groups[i].Students[j].Name} " +
                                          $"{ groups[i].Students[j].Surname} " +
                                          $"{ groups[i].Students[j].Age} " +
                                          $"{ groups[i].Students[j].Email}");
                    }
                    Console.WriteLine();
                }
                else
                    Console.WriteLine();
            }
        }

        static void Print(Teacher[] teachers)
        {
            for (int i = 0; i < teachers.Length; i++)
            {
                if (teachers[i] != null)
                    Console.WriteLine($"{teachers[i].FullName}");
                else
                    Console.WriteLine();
            }
        }

        static void Print(Student[] students)
        {
            for (int i = 0; i < students.Length; i++)
            {
                if (students[i] != null)
                    Console.WriteLine($"{students[i].FullName} {students[i].Age}");
                else
                    Console.WriteLine();
            }
        }
    }
}
